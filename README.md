# OctoPrint-FilamentMovement

This plugin tracks the movement of filament, in real time during a 3D print job. To be useful, it
needs a hardware [Filament Movement Sensor](https://gitlab.com/coofercat/filament-movement-sensor).

The hardware sensor can optionally connect to the extruder stepper motor to track "ticks", can
optionally provide a hardware filament run-out sensor and is connected to Octoprint via a USB
cable (over which, communication is Gcode-like).

In cases where Octoprint is sending Gcode to the printer (ie. not printing from SD card), this
plugin can watch the Gcode being sent and track the requested extrusion. This, plus stepper motor
information and physical filament movement information is plotted on a graph in the Octoprint UI.

## Setup

Install via the bundled [Plugin Manager](https://github.com/foosel/OctoPrint/wiki/Plugin:-Plugin-Manager)
or manually using this URL:

    https://gitlab.com/coofercat/filament-movement-sensor/-/releases/


**TODO:** Describe how to install your plugin, if more needs to be done than just installing it via pip or through
the plugin manager.

## Configuration

**TODO:** Describe your plugin's configuration options (if any).

## TODO

Things left to do in the plugin:

* Ensure connect/disconnect work properly. At present, disconnecting from a sensor causes errors which we don't
  recover from.

* Other bug fixes - there must be plenty?

* Pretty-up the graph a bit, add axis labels, etc

* Measure difference between "requested" and "actual", optionally graph that rather than mm/s as now

* Be able to pause the print if requested/actual diverge too much for too long. This will need testing with
  a poorly extruded print job, filament actually grinding in the extruder and filament run-out, and maybe
  a model which consitently under-extrudes at some point during the print.

* See if there's a nice way to "flatten" the gcode graph. Eg. a move which extrudes 10mm of filament over 10s
  will be reported as 5mm/s in the 2s graph update, and then 0mm/s for the remainder of the move. This would
  ideally show 1mm/s for 10s instead.

* Provide ways to calibrate and otherwise setup the harware sensor from within the plugin
