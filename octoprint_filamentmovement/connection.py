import os
import re
import sys
import glob
import threading, queue
import serial
import serial.tools.list_ports
import time, datetime

class PrintStates:
	printing = 1
	paused = 2
	stopped = 3

class Connection(object):
	def __init__(self, plugin):
		self.plugin = plugin
		self._logger = plugin._logger
		self._printer = plugin._printer
		self._printer_profile_manager = plugin._printer_profile_manager
		self._plugin_manager = plugin._plugin_manager
		self._identifier = plugin._identifier
		self._settings = plugin._settings

		self.extruder_mode_absolute = False

		self.event = threading.Event()
		self.read_thread = None
		self.write_thread = None
		self.update_thread = None

		self.stepper_forward = 0.0
		self.stepper_backward = 0.0
		self.filament_forward = 0.0
		self.filament_backward = 0.0
		self.gcode_forward = 0.0
		self.gcode_backward = 0.0
		self.gcode_absolute = 0.0

		self.print_status = PrintStates.stopped

		self.last_absolute_readings = None

		self.previous_updates = []

		self.write_thread_queue = queue.Queue()

		self.ports = []

		self.dynamic_config()		

		self.serialConn = None

		self._logger.info("Connection object starting up")

		self._connected = False

	def dynamic_config(self):
		self.graph_keys = ['filament_forward','filament_backward']
		if self._settings.get(["stepper_connection"]):
			self.graph_keys.extend(['stepper_forward','stepper_backward'])
		if self._settings.get(["gcode_counter"]):
			self.graph_keys.extend(['gcode_forward','gcode_backward'])
    		

	def save_update(self, **kwargs):
		row = {}
		for key, value in kwargs.items():
			row[key] = value
		if 'timestamp' not in kwargs:
			row['timestamp'] = datetime.datetime.now()

		self.previous_updates.append(row)
		if len(self.previous_updates) > 900:
			self.previous_updates.pop(0)

	def get_last_update(self, index=-1):
		if len(self.previous_updates) == 0:
			return (None, None)
		out = []
		for key in self.graph_keys:
			out.append(self.previous_updates[index][key])

		return (out, self.previous_updates[index]['timestamp'])


	def _calculate_mm_per_second(self, a, b, a_time, b_time):
		return (b - a) / (b_time - a_time).total_seconds()

	def calculate_and_save_update(self):
		if self.last_absolute_readings is not None:
			out = {}
			now = datetime.datetime.now()
			for item in self.graph_keys:
				out[item] = self._calculate_mm_per_second(self.last_absolute_readings[item], getattr(self, item), self.last_absolute_readings['timestamp'], now)
			self.save_update(**out)

		self.last_absolute_readings = {
			'stepper_forward':  self.stepper_forward,
			'stepper_backward': self.stepper_backward,
			'filament_forward': self.filament_forward,
			'filament_backward': self.filament_backward,
			'gcode_forward': self.gcode_forward,
			'gcode_backward': self.gcode_backward,
			'timestamp': datetime.datetime.now(),
		}
		

	def connect(self):
		if self._connected:
			return

		self._logger.info("Connecting...")

		self.ports = self.getAllPorts()
		self._logger.info("Potential ports: %s" % self.ports)
		if len(self.ports) > 0:
			for port in self.ports:
				if not self._connected:
					if self.isPrinterPort(port):
						self._logger.info("Skipping Printer Port:" + port)
					else:
						try:
							self.serialConn = serial.Serial(port, 115200)
							self._logger.info("Starting read thread...")
							self.start_threads()
							self._connected = True
							self.update_ui_connection("Connected")
						except serial.SerialException:
							self.update_ui_error("Connection failed!")
			if not self._connected:
				self.update_ui_error("Couldn't connect on any port.")
		else:
			msg = "NO SERIAL PORTS FOUND!"
			self.update_ui_error(msg)

	def disconnect(self):
		if not self._connected:
			return
		self.write_thread_queue.put("")
                self.event.set()
		self._connected = False
		self.serialConn.close();
		self.serialConn = None
		self.update_ui_connection("Disconnected")

	def reset_sensor(self):
		if not self._connected:
			return
		self.write_thread_queue.put("G28\n")
		self.gcode_forward = 0.0
		self.gcode_backward = 0.0
		self.gcode_absolute = 0.0

	def send_initialiser(self):
		if self._connected:
			self.update_ui_connection('Connected')
		else:
			self.update_ui_connection('Disconnected')

		for i in range(0, len(self.previous_updates)):
			(last_update, last_update_timestamp) = self.get_last_update(i)
			self._plugin_manager.send_plugin_message(self._identifier, dict(
				type="update",
				graph_update_time = last_update_timestamp.isoformat(),
				graph_data_y = last_update,
			))

	
	def update_ui_connection(self, state):
		self._plugin_manager.send_plugin_message(self._identifier, dict(
                	type="connection",
			state=state,
		))
		
	def update_ui_data(self):
		(last_update, last_update_timestamp) = self.get_last_update()
		update = dict(
			type="update",
			filament_forward = format(float(self.filament_forward), '.2f'),
			filament_backward = format(float(self.filament_backward), '.2f'),
			graph_update_time = last_update_timestamp.isoformat(),
			graph_data_y = last_update,
		)

		if self._settings.get(["stepper_connection"]):
    			update['stepper_forward'] = format(float(self.stepper_forward), '.2f')
    			update['stepper_backward'] = format(float(self.stepper_backward), '.2f')
    		else:
    			update['stepper_forward'] = '-'
    			update['stepper_backward'] = '-'
    		if self._settings.get(["gcode_counter"]):
    			update['gcode_forward'] = format(float(self.gcode_forward), '.2f')
    			update['gcode_backward'] = format(float(self.gcode_backward), '.2f')
    		else:
    			update['gcode_forward'] = '-'
    			update['gcode_backward'] = '-'

		self._plugin_manager.send_plugin_message(self._identifier, update)

	def monitor_gcode_extrusion(self, amount):
		try:
			amount = float(amount)
		except Exception as e:
			self._logger.info("Gcode amount Exception for '%s': %s" % (amount, e))
			return

		try:
			# Manual moves aren't line numbered, and so are assumed to be relative
			# Manual moves are only possible when not printing, so...
			if self.extruder_mode_absolute:
				if amount < 0:
					self.gcode_backward = self.gcode_backward + abs(amount)
				elif amount < self.gcode_absolute:
					self.gcode_backward = self.gcode_backward + (self.gcode_absolute - amount)
				else:
					self.gcode_forward = self.gcode_forward + (amount - self.gcode_absolute)
				self.gcode_absolute = amount
			else:
				if amount < 0:
					self.gcode_backward = self.gcode_backward + abs(amount)
				else:
					self.gcode_forward = self.gcode_forward + amount
		except Exception(e):
			self._logger.info("Exception during gcode counting: %s" % (e))

	def set_extruder_mode(self, gcode):
		if gcode == "M83" or gcode == "G91":
			self._logger.info("Seen %s - switching to relative extruder mode" % (gcode))
			self.extruder_mode_absolute = False
		elif gcode == "M82" or gcode == "G90":
			self._logger.info("Seen %s - switching to absolute extruder mode" % (gcode))
			self.extruder_mode_absolute = True

	def set_extruder_position(self, position):
		self._logger.info("Seen G92 - setting extruder position to %s" % (position))
		self.gcode_absolute = float(position)

	# Tracking printer state isn't actually used right now
	def print_started(self):
		self.print_status = PrintStates.printing

	def print_stopped(self):
		self.print_status = PrintStates.stopped

	def print_paused(self):
		self.print_status = PrintStates.paused
		

	# below code "stolen" from https://gitlab.com/mosaic-mfg/palette-2-plugin/blob/master/octoprint_palette2/Omega.py
	def getAllPorts(self):
		baselist = []

		if 'win32' in sys.platform:
			# use windows com stuff
			self._logger.info("Using a windows machine")
			for port in serial.tools.list_ports.grep('.*0403:6015.*'):
				self._logger.info("got port %s" % port.device)
				baselist.append(port.device)

		baselist = baselist + glob.glob('/dev/serial/by-id/*FTDI*') + glob.glob('/dev/*usbserial*') + glob.glob(
			'/dev/*usbmodem*')
		baselist = self.getRealPaths(baselist)
		# get unique values only
		baselist = list(set(baselist))
		return baselist

	def getRealPaths(self, ports):
		self._logger.info("Paths: %s" % ports)
		for index, port in enumerate(ports):
			port = os.path.realpath(port)
			ports[index] = port
		return ports

	def isPrinterPort(self, selected_port):
		selected_port = os.path.realpath(selected_port)
		printer_port = self._printer.get_current_connection()[1]
		self._logger.info("Trying port: %s" % selected_port)
		self._logger.info("Printer port: %s" % printer_port)
		# because ports usually have a second available one (.tty or .cu)
		printer_port_alt = ""
		if printer_port is None:
			return False
		else:
			if "tty." in printer_port:
				printer_port_alt = printer_port.replace("tty.", "cu.", 1)
			elif "cu." in printer_port:
				printer_port_alt = printer_port.replace("cu.", "tty.", 1)
			self._logger.info("Printer port alt: %s" % printer_port_alt)
			if selected_port == printer_port or selected_port == printer_port_alt:
				return True
			else:
				return False

	def sensor_read_thread(self, serialConnection):
		self._logger.info("Read thread starting up")
		while(not self.event.isSet()):
			try:
				line = serialConnection.readline()
				if line:
					line = line.strip()
					self._logger.debug("Received from sensor: %s" % (line))
					if line[:4] == "echo":
						self._logger.info("Sensor said: %s" % (line))
					elif line == 'ok':
						# swallow this - probably the response to some previous command
						# TODO: have some way to tie this up to a previously written request
						pass
					elif line[:2] == 'ok':
						# Regex this line to see if it's one of ours
						# Looking for: ok EF:0.00 EB:0.00 FF:0.00 FB:0.00
						p = re.compile('^ok\sEF:(\d*\.\d*)\sEB:(\d*\.\d*)\sFF:(\d*\.\d*)\sFB:(\d*\.\d*)\s*$')
						out = p.search(line)
						self._logger.debug("Sensor response: %s" % (line))
						try:
							(a,b,c,d) = out.groups(0)
						except Exception as e:
							self._logger.info("Regex exception: %s" % (e))

						try:
							self.stepper_forward = float(a)
							self.stepper_backward = float(b)
							self.filament_forward = float(c)
							self.filament_backward = float(d)
						except Exception as e:
							self._logger.info("String to float exception: %s on (%s,%s,%s,%s)" % (e,a,b,c,d))
			except serial.SerialException as e:
				self._logger.debug("Serial read exception: %s - telling threads to stop" % (e))
				self.disconnect()
			try:
				self.calculate_and_save_update()
				self.update_ui_data()
			except Exception as e:
				self._logger.info("Exception sending client update: %s" % (e))

		self._logger.info("Read thread stopped")
		self.read_thread = None

	def sensor_write_thread(self, serialConnection):
		self._logger.info("Write thread starting up")
		try:
		  while(not self.event.isSet()):
			try:
				# This blocks, stopping the thread from exiting when told to :-(
				# To mitigate, we're told to quit via an empty message on the queue
				item = self.write_thread_queue.get()
				if item != "":
					serialConnection.write(item.encode())
					self._logger.debug("Written to sensor: %s" % (item.rstrip))
			except serial.SerialException as e:
                                self._logger.info("Serial write exception: %s - disconnecting" % (e))
                                self.disconnect()
		except Exception as e:
			self._logger.info("Write thread exception: %s" % (e))
		self._logger.info("Write thread stopped")
		self.write_thread = None

	def sensor_update_thread(self):
		self._logger.info("Update thread starting up")
		time.sleep(2)
		try:
		  while(not self.event.isSet()):
			self.write_thread_queue.put("M114\n")
			self._logger.debug("Asking sensor for an update...")
			time.sleep(2)
		except Exception as e:
			self._logger.info("Update exception: %s" % (e))
		self._logger.info("Update thread stopped")
		self.update_thread = None

	def start_threads(self):
		self.event = threading.Event()
		if self.read_thread is None:
			self.read_thread = threading.Thread(
				target=self.sensor_read_thread,
				args=(self.serialConn,),
			)
			self.read_thread.start()
		if self.write_thread is None:
                        self.write_thread = threading.Thread(
                                target=self.sensor_write_thread,
                                args=(self.serialConn,),
                        )
                        self.write_thread.start()
		if self.update_thread is None:
                        self.update_thread = threading.Thread(
                                target=self.sensor_update_thread,
                                args=(),
                        )
                        self.update_thread.start()
