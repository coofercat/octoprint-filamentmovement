$(function() {
    function FilamentMovementViewModel(parameters) {
        var self = this;

        self.loginState = parameters[0];
        self.settings = parameters[1];

	self.max_graph_points = 900;

	self.stepper_forward = ko.observable();
	self.stepper_backward = ko.observable();
	self.filament_forward = ko.observable();
	self.filament_backward = ko.observable();
	self.gcode_forward = ko.observable();
	self.gcode_backward = ko.observable();

	self.sensor_connection = ko.observable();

	self.layout = {
                autosize: true,
                showlegend: false,
                /* legend: {"orientation": "h"}, */
                xaxis: { type: "date",tickformat:"%H:%M:%S", automargin: true, title: {standoff: 0},linecolor: 'black', linewidth: 2, mirror: true},
                yaxis: { type:"linear", automargin: true, title: {text: "mm/s", standoff: 0},linecolor: 'black', linewidth: 2, mirror: true },
                margin: {l:35,r:30,b:0,t:20,pad:5},
                images: [{"source": "/static/img/graph-background.png",
                        "xref": "paper",
                        "yref": "paper",
                        "x": 0.5,
                        "y": 0.5,
                        "sizex": 0.75,
                        "sizey": 0.75,
                        "xanchor": "center",
                        "yanchor": "middle",
                        "layer": "above"}]
                };
        self.options = {
                showLink: false,
                sendData: false,
                displaylogo: false,
                displayModeBar: true,
                editable: false,
                showTips: false
        };

	self.data = []
	self.mutlipliers = []

	self.dynamic_config = function() {
    	    multiplier = 1
            if(self.settings.settings.plugins.filamentmovement.backwards_is_negative()) {
                multiplier = -1;
            }
            self.data = [
                { 'x': [], 'y': [], 'name': 'Filament Forward' },
                { 'x': [], 'y': [], 'name': 'Filament Backward' },
            ]
            self.multipliers = [ 1, multiplier ];
            if(self.settings.settings.plugins.filamentmovement.stepper_connection()) {
		self.data.push({ 'x': [], 'y': [], 'name': 'Stepper Forward' });
 		self.data.push({ 'x': [], 'y': [], 'name': 'Stepper Backward' });
		self.multipliers.push(1, multiplier);
            }
	    if(self.settings.settings.plugins.filamentmovement.gcode_counter()) {
                self.data.push({ 'x': [], 'y': [], 'name': 'GCode Forward' });
                self.data.push({ 'x': [], 'y': [], 'name': 'GCode Backward' });
                self.multipliers.push(1, multiplier);
            }
	};

        // This will get called before the FilamentMovementViewModel gets bound to the DOM, but after its depedencies have
        // already been initialized. It is especially guaranteed that this method gets called _after_ the settings
        // have been retrieved from the OctoPrint backend and thus the SettingsViewModel been properly populated.
        self.onBeforeBinding = function() {
            self.stepper_forward(0.0);
            self.stepper_backward(0.0);
            self.filament_forward(0.0);
            self.filament_backward(0.0);
            self.gcode_forward(0.0);
            self.gcode_backward(0.0);

            self.dynamic_config();
	}

	self.onStartupComplete = function() {
            try {
                self.ajaxRequest({"command": "initialise"});
            } catch(err) {
		console.log("Ajax call error: " + err);
            }

            PLOTLY = document.getElementById('filamentmovementsensorplotly');
            self.plotly = PLOTLY;
            Plotly.newPlot( self.plotly, self.data, self.layout, self.options);
        };

	self.ajaxRequest = payload => {
            return $.ajax({
                url: API_BASEURL + "plugin/filamentmovement",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(payload),
                contentType: "application/json; charset=UTF-8"
            });
        };

	self.onDataUpdaterPluginMessage = (pluginIdent, message) => {
            if (pluginIdent === "filamentmovement") {
		if (message.type === "update") {
			try {
				self.stepper_forward(message.stepper_forward);
                        	self.stepper_backward(message.stepper_backward);
                        	self.filament_forward(message.filament_forward);
                        	self.filament_backward(message.filament_backward);
                        	self.gcode_forward(message.gcode_forward);
                        	self.gcode_backward(message.gcode_backward);
			} catch(err) {
				console.log(err.name + ": Could not get absolute value because: " + err.message);
			}

			try {
				max = message.graph_data_y.length;
				x = new Date(message.graph_update_time);
				for(var i = 0; i < max; i++) {
    					y = parseFloat(message.graph_data_y[i]) * self.multipliers[i];
					self.data[i]['x'].push(x);
					self.data[i]['y'].push(y);
					if(self.data[i]['x'].length > self.max_graph_points) {
						//self.data[i].shift();
						self.data[i]['x'].shift();
						self.data[i]['y'].shift();
					}
				}
				Plotly.update(self.plotly, self.data, self.layout);
			}
			catch(err) {
				console.log("FAIL: " + err.message);
			}

		} else if(message.type === "connection") {
			self.sensor_connection(message.state)

		} else {
			console.log('Ignoring plugin message type ' + message.type);
		}
	    } 
	};
	self.onEventSettingsUpdated = (payload) => {
		self.dynamic_config();
		self.ajaxRequest({"command": "initialise"});
	};
    }

    // This is how our plugin registers itself with the application, by adding some configuration information to
    // the global variable ADDITIONAL_VIEWMODELS
    ADDITIONAL_VIEWMODELS.push([
        // This is the constructor to call for instantiating the plugin
        FilamentMovementViewModel,

        // This is a list of dependencies to inject into the plugin, the order which you request here is the order
        // in which the dependencies will be injected into your view model upon instantiation via the parameters
        // argument
        ["loginStateViewModel", "settingsViewModel"],

        // Finally, this is the list of all elements we want this view model to be bound to.
        [document.getElementById("tab_plugin_filamentmovement")]
    ]);
});

