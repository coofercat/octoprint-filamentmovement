# coding=utf-8
from __future__ import absolute_import

import octoprint.plugin
import flask
import re

from . import connection

EXTRUSION_EXPRESSION = "\s*G[0|1|2|3].*E[+]*([\\-0-9]+[.]*[0-9]*).*"
extrusionPattern = re.compile(EXTRUSION_EXPRESSION)

EXTRUDER_MODE_EXPRESSION = "^\s*((M8[2|3])|(G9[0|1]))\s*$"
extruder_mode_pattern = re.compile(EXTRUDER_MODE_EXPRESSION)

EXTRUDER_POSITION_EXPRESSION = "^\s*G92.*E[+]*([\\-0-9]+[.]*[0-9]*).*$"
extruder_position_pattern = re.compile(EXTRUDER_POSITION_EXPRESSION)

class FilamentMovementPlugin(octoprint.plugin.StartupPlugin,
                       octoprint.plugin.TemplatePlugin,
                       octoprint.plugin.SettingsPlugin,
                       octoprint.plugin.AssetPlugin,
                       octoprint.plugin.SimpleApiPlugin,
                       octoprint.plugin.EventHandlerPlugin):

	def on_after_startup(self):
		self._logger.info("Filament Movement Sensor plugin starting up")
		
		self.stepper_forward = 0.0
		self.stepper_backward = 0.0
		self.filament_forward = 0.0
		self.filament_backward = 0.0
		self.gcode_forward = 0.0
		self.gcode_backward = 0.0

		self.conn = connection.Connection(self)

		printer_state = self._printer.get_state_id()

		self._logger.debug("Printer state is %s" % (printer_state))

		# See https://docs.octoprint.org/en/master/modules/printer.html#octoprint.printer.PrinterInterface.get_state_id
		if self._settings.get(["auto_connect_disconnect"]) and printer_state in ['OPERATIONAL', 'PRINTING', 'PAUSED']:
			# Only attempt to connect if the printer is connected
			self.conn.connect()

	def get_settings_defaults(self):
		return dict(
			stepper_connection=True,
			gcode_counter=True,
			auto_connect_disconnect=True,
			reset_sensor_on_print=True,
			backwards_is_negative=True,
		)

	def get_template_configs(self):
		return [
			dict(type="settings", custom_bindings=False)
		]

	def get_assets(self):
		return dict(
			js=["js/filamentmovement.js"],
			css=["css/filamentmovement.css"],
			less=["less/filamentmovement.less"]
		)

	def update_ui(self):
		self._plugin_manager.send_plugin_message(self._identifier, dict(
			stepper_forward=self.stepper_forward,
			stepper_backward=self.stepper_backward,
			filament_forward=self.filament_forward,
			filament_backward=self.filament_backward,
			gcode_forward=self.gcode_forward,
			gcode_backward=self.gcode_backward,
		))

	def on_event(self, event, payload, **kwargs):
                try:
                        if event == 'Connected':
				self._logger.info("Printer connected")
				if self._settings.get(["auto_connect_disconnect"]):
                                	# Connect to the filament movement sensor too
                                	if self.conn:
                                        	self.conn.connect()
                        elif event == 'Disconnected':
				self._logger.info("Printer disconnected")
				if self._settings.get(["auto_connect_disconnect"]):
                                	# Disconnect from the filament sensor too
                                	if self.conn:
                                        	self.conn.disconnect()
			elif event == 'PrintStarted':
				self._logger.info("Print started")
				if self._settings.get(["reset_sensor_on_print"]):
					# Reset the sensor
					if self.conn:
						self.conn.reset_sensor()
				if self.conn:
					self.conn.print_started()
			elif event in ['PrintFailed', 'PrintDone', 'PrintCancelled']:
				if self.conn:
					self.conn.print_stopped()
			elif event == 'PrintPaused':
				if self.conn:
					self.conn.print_paused()
			elif event == 'SettingsUpdated':
    				self._logger.info("Settings Updated")
    				if self.conn:
        				self.conn.dynamic_config()

                except Exception as e:
                        self._logger.info("Event Exception %s" % (e))

	def get_api_commands(self):
		return dict(
			connect=[],
			disconnect=[],
			reset=[],
			initialise=[],
		)

	def on_api_command(self, command, payload):
		try:
			if command == 'connect':
				if self.conn:
					self.conn.connect()
			elif command == 'disconnect':
				if self.conn:
					self.conn.disconnect();
			elif command == 'reset':
				if self.conn:
					self.conn.reset_sensor();
			elif command == 'initialise':
				if self.conn:
					self.conn.send_initialiser();

			response = "POST request (%s) successful" % command
			return flask.jsonify(response=response, data=None, status=200), 200
		except Exception as e:
			self._logger.info("Exception message: %s" % str(e))
			return flask.jsonify(error=str(e), status=500), 500

	def process_gcode_line(self, comm_instance, phase, cmd, cmd_type, gcode, subcode=None, tags=None, *args, **kwargs):
		try:
			match = extrusionPattern.match(cmd)
			if match:
				self.conn.monitor_gcode_extrusion(match.group(1))
			else:
				match = extruder_position_pattern.match(cmd)
				if match:
					self.conn.set_extruder_position(match.group(1))
				else:
					match = extruder_mode_pattern.match(cmd)
					if match:
						self.conn.set_extruder_mode(match.group(1))
		except AttributeError:
			self._logger.debug("Attempting to clock up gcode, but are not connected")
		except Exception as e:
			self._logger.info("Attempting to clock up gcode, Exception: %s" % (e))

__plugin_name__ = "Filament Movement Sensor"
__plugin_pythoncompat__ = ">=2.7,<4"
__plugin_implementation__ = FilamentMovementPlugin()

def __plugin_load__():
	global __plugin_implementation__
	__plugin_implementation__ = FilamentMovementPlugin()

	global __plugin_hooks__
	__plugin_hooks__ = {
		"octoprint.comm.protocol.gcode.sent": __plugin_implementation__.process_gcode_line
	}
